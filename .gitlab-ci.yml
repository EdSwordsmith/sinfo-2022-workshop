# This is the Gitlab CI/CD configuration file.
#
# A pipeline is composed of independent jobs that run scripts, grouped into stages.
# Stages run in sequential order, but jobs within stages run in parallel.
#
# For more information, see: https://docs.gitlab.com/ee/ci/yaml/index.html

image: ubuntu:latest

before_script:
  - apt-get -yq update
  - apt-get -yq install software-properties-common
  - add-apt-repository -y ppa:deadsnakes/ppa
  - apt-get -yq update
  - apt-get -yq install python3.7 python3.7-dev python3-pip python3-venv
  - apt-get -yq install git
  - git config --global user.email "cicd@email.com"
  - git config --global user.name "CI/CD"

stages: # List of stages for jobs, and their order of execution
  - build
  - test
  - deploy

build-job:
  stage: build
  script:
    - cp build.ini.sample build.ini
    - sed -i "s/@BUILD_USER@/${GITLAB_USER_NAME}/g" build.ini
    - sed -i "s/@BUILD_TIME@/$(date)/g" build.ini
  artifacts:
    paths:
      - build.ini

unit-test-job: # This job runs in the test stage.
  stage: test
  script:
    - echo "Running unit tests..."
    - python3 -m venv env
    - source env/bin/activate
    - pip install -r requirements.txt
    - pytest
    - echo "All unit tests passed."

lint-test-job:
  stage: test
  script:
    - python3 -m venv env
    - source env/bin/activate
    - pip install -r requirements.txt
    - pylint *.py

deploy-job: # This job runs in the deploy stage.
  stage: deploy # It only runs when *all* jobs in the test stage complete successfully.
  dependencies:
    - build-job
  script:
    - git add "build.ini"
    - git commit -m "Adding 'build.ini'"
    - echo "Deploying application..."
    - git remote add heroku https://heroku:${HEROKU_API_KEY}@git.heroku.com/${HEROKU_APP_NAME}.git
    - git push -f heroku HEAD:refs/heads/master
    - echo "Application successfully deployed."
